import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:newsapp/AppConstants/colors.dart';
import 'package:newsapp/AppConstants/customTheme.dart';
import 'package:newsapp/Episodes/episodesGridList.dart';
import 'package:newsapp/Home/homeScreen.dart';
import 'package:newsapp/Profile/profileScreen.dart';
import 'package:newsapp/Provider/ThemeNotifier.dart';
import 'package:newsapp/Search/searchScreen.dart';
import 'package:newsapp/Setting/settingScreen.dart';
import 'package:provider/provider.dart';

class BottomNavigationBarScreen extends StatefulWidget {
  @override
  _BottomNavigationBarScreenState createState() =>
      _BottomNavigationBarScreenState();
}

class _BottomNavigationBarScreenState extends State<BottomNavigationBarScreen> {
  List<Widget> _childrenList = [
    EpisodesGridList(),
    SearchScreen(),
    ProfileScreen(),
    SettingScreen()
  ];
  int _currentIndex = 0;
  var _darkTheme = true;
  var themeNotifier;

  @override
  Widget build(BuildContext context) {
    themeNotifier = Provider.of<ThemeNotifier>(context);
    _darkTheme = (themeNotifier.getTheme() == darkTheme);
    return Scaffold(
      backgroundColor: _darkTheme ? Colors.transparent : Colors.white,
      bottomNavigationBar: Container(
        color: _darkTheme ? Colors.transparent : Colors.white,
        child: Container(
          decoration: BoxDecoration(
            color: _darkTheme ? Colors.black87 : Colors.white,
            boxShadow: [
              BoxShadow(
                  color: _darkTheme ? Colors.white12 : Colors.grey[400],
                  offset: Offset(2, 3),
                  blurRadius: 5,
                  spreadRadius: 4)
            ],
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(20),
              topLeft: Radius.circular(20),
            ),
          ),
          child: BottomNavigationBar(
            backgroundColor: Colors.transparent,
            currentIndex: _currentIndex,
            elevation: 0,
            onTap: onTabTapped,
            iconSize: 20,
            selectedFontSize: 14,
            selectedItemColor: lightblue,
            unselectedItemColor: _darkTheme ? Colors.white54 : Colors.grey[600],
            unselectedFontSize: 12,
            type: BottomNavigationBarType.fixed,
            items: [
              BottomNavigationBarItem(
                icon: Icon(FontAwesomeIcons.home),
                activeIcon: Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                        color: Colors.blue, shape: BoxShape.circle),
                    child: Icon(
                      FontAwesomeIcons.home,
                      color: Colors.white,
                    )),
                title: Container(),
              ),
              BottomNavigationBarItem(
                  icon: Icon(FontAwesomeIcons.search),
                  activeIcon: Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                          color: Colors.blue, shape: BoxShape.circle),
                      child: Icon(
                        FontAwesomeIcons.search,
                        color: Colors.white,
                      )),
                  title: Container()),
              BottomNavigationBarItem(
                  icon: Icon(FontAwesomeIcons.userAlt),
                  activeIcon: Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                          color: Colors.blue, shape: BoxShape.circle),
                      child: Icon(
                        FontAwesomeIcons.userAlt,
                        color: Colors.white,
                      )),
                  title: Container()),
              BottomNavigationBarItem(
                  icon: Icon(FontAwesomeIcons.cog),
                  activeIcon: Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                          color: Colors.blue, shape: BoxShape.circle),
                      child: Icon(
                        FontAwesomeIcons.cog,
                        color: Colors.white,
                      )),
                  title: Container()),
            ],
          ),
        ),
      ),
      body: _childrenList[_currentIndex],
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}

import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:newsapp/AppConstants/customTheme.dart';
import 'package:newsapp/Provider/ThemeNotifier.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> with TickerProviderStateMixin {
  TabController _tabController;
  TextEditingController searchController = new TextEditingController();
  var _darkTheme = true;
  List<String> networkImage = [
    "https://picsum.photos/200/300",
    "https://picsum.photos/200",
    "https://picsum.photos/200",
    "https://picsum.photos/200",
    "https://picsum.photos/200/400",
  ];
  void initState() {
    _tabController = new TabController(length: 5, vsync: this);
    super.initState();
  }
  var themeNotifier;
  @override
  Widget build(BuildContext context) {
    themeNotifier = Provider.of<ThemeNotifier>(context);
    _darkTheme = (themeNotifier.getTheme() == darkTheme);
    return Scaffold(
      backgroundColor:Colors.transparent  ,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text("Episodes", style: TextStyle(
            fontSize: 25, fontWeight: FontWeight.bold, color: Colors.blue),
        ),

      ),
      body: _body(),
    );
  }


  _body(){
    return Column(
      children: [
        _upperBody(),
        Expanded(child:
        _tabbody())
      ],
    );
  }

  _upperBody(){
    return Container(
      padding: EdgeInsets.fromLTRB(10, 20, 30, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container()
,
          _imageIcon()
         , Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Your Name",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600,fontSize: 25),)
              ,SizedBox(
                height: 2,
              )
              ,Text("Bio",style: TextStyle(color: Colors.grey[700],fontWeight: FontWeight.w600,fontSize: 18),),
              SizedBox(
                height: 30,
              )
            ],
          ),
          Container()
        ],
      ),
    );
  }

  _imageIcon(){
    return Column(
      children: [
        Row(
          children: [
            ClipRRect(
borderRadius: BorderRadius.circular(90),
              child: Image.asset("assets/images/img3.jpg",height: 95,width: 95,fit: BoxFit.cover,),
            ),
          ],
        ),
        SizedBox(height: 10,),
        Row(
          children: [
            Text("Edit",style: TextStyle(color:Colors.blue,fontSize: 16,fontStyle: FontStyle.italic),),
          Icon(Icons.edit,color: Colors.blue,)
          ],
        )
      ],
    );
  }

  _tabbody() {
    return Container(

      child: DefaultTabController(
        length: 5,
        child: Scaffold(
          backgroundColor: Colors.transparent,

          appBar: PreferredSize(
            preferredSize: Size.fromHeight(200),
            child: TabBar(
              indicator: new BubbleTabIndicator(
                indicatorHeight: 30.0,
                indicatorColor: Colors.grey,
                tabBarIndicatorSize: TabBarIndicatorSize.tab,
              ),
//              indicatorPadding: EdgeInsets.all(30),
              labelPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              controller: _tabController,
              isScrollable: true,
              indicatorColor: Colors.transparent,
              unselectedLabelColor: Colors.grey,
              labelStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
              unselectedLabelStyle:
              TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
              indicatorSize: TabBarIndicatorSize.tab,
              labelColor: _darkTheme ? Colors.white:Colors.black,
              indicatorWeight: 2,
              tabs: <Widget>[
                Container(
//                  margin: EdgeInsets.fromLTRB(0, 15, 5, 15),
                  child: Text(
                    "Shared",
//                  style: TextStyle(
//                      fontSize: 18, fontWeight: FontWeight.w700),
                  ),
                ),
                Container(
//                  padding: EdgeInsets.fromLTRB(0, 15, 5, 15),
                  child: Text(
                    "BookMark",
                  ),
                ),
                Container(
//                  padding: EdgeInsets.fromLTRB(0, 15, 5, 15),
                  child: Text(
                    "Point of Interest",
                  ),
                ),
                Container(
//                  padding: EdgeInsets.fromLTRB(0, 15, 5, 15),
                  child: Text(
                    "Starred",
                  ),
                ),
                Container(
//                  padding: EdgeInsets.fromLTRB(0, 15, 5, 15),
                  child: Text(
                    "Draft",
                  ),
                ),
              ],
            ),
          ),
          body: TabBarView(
            dragStartBehavior: DragStartBehavior.down,
            controller: _tabController,
            children: <Widget>[
//              _inboxContainer(),
              _topResult(),
              Center(
                child: Container(
                  child: Text("hi "),
                ),
              ),
              Center(
                child: Container(
                  child: Text("hi "),
                ),
              ),
              Center(
                child: Container(
                  child: Text("hi "),
                ),
              ),
              Center(
                child: Container(
                  child: Text("hi "),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  _topResult() {
    return Container(
      child: ListView.builder(
          itemCount: 5,
          itemBuilder: (BuildContext context, int index) {
            return itemView(index);
          }),
    );
  }
  itemView(int index) {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
      decoration: BoxDecoration(
          color:_darkTheme?Colors.white12: Colors.blueGrey.shade50,
          borderRadius: BorderRadius.circular(20)
      ),
      child: Row(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(5, 5, 0, 5),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Image.network(
                networkImage[index],
                height: 100,
                width: 100,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Headline ",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25,color: _darkTheme?Colors.white70:Colors.black),
                  ),
                  Text(
                    "Lorem ipsum dolor sitamet, consetetursadipscing elitr, seddiam nonumy eirmod",
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        color: Colors.grey[600]),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:newsapp/AppConstants/appConstants.dart';

class SportsScreen extends StatefulWidget {
  @override
  _SportsScreenState createState() => _SportsScreenState();
}

class _SportsScreenState extends State<SportsScreen> {
  List<Item> itemList = List();

  @override
  void initState() {
    // TODO: implement initState

    itemList.add(Item(text: "Sports", image: "assets/images/bike.png"));
    itemList.add(Item(text: "Science", image: "assets/images/atom.png"));
    itemList.add(Item(text: "Technology", image: "assets/images/computer.png"));
    itemList.add(Item(text: "Buisness", image: "assets/images/stats.png"));
    itemList.add(Item(text: "Beauty", image: "assets/images/makeup.png"));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppConstants.appbar("Episodes"),
      body: _body(),
    );
  }

  _body() {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.black45,
                offset: Offset(1, 2),
                blurRadius: 10,
                spreadRadius: 2)
          ],
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20))),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(15),
            child: Row(
              children: [
                Container(
                  height:50,width: 50,
                    margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                    decoration: BoxDecoration(
                      color: Colors.blueGrey[50],
                      image: DecorationImage(
                          image: NetworkImage(
                            "https://picsum.photos/200/300",
                          ),
                          fit: BoxFit.cover),
                      shape: BoxShape.circle,
                    ))

              ,SizedBox(
                  width: 20,
                ) , Container(


                  child: Text(
                    "Sports"
                  ,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18,letterSpacing: 1),
                  ),
                )
              ],
            ),
          )

          ,Expanded(child:
         _gridList()
          )
        ],
      ),
    );
  }

  _gridList() {
    return Container(

      child: GridView.builder(
//          padding: EdgeInsets.all(10),
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 1,
              mainAxisSpacing: 1,
              childAspectRatio: 1.4),
          itemCount: itemList.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
              decoration: BoxDecoration(
                  color: Colors.blueGrey[50],
                  image: DecorationImage(
                      image: NetworkImage(
                        "https://picsum.photos/200/300",
                      ),
                      fit: BoxFit.cover),
                  borderRadius: BorderRadius.circular(20)),
            );
          }),
    );
  }
}

class Item {
  String image, text;

  Item({this.image, this.text});
}

import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:newsapp/AppConstants/appConstants.dart';
import 'package:newsapp/AppConstants/colors.dart';

class LocationSearchScreen extends StatefulWidget {
  @override
  _LocationSearchScreenState createState() => _LocationSearchScreenState();
}

class _LocationSearchScreenState extends State<LocationSearchScreen> {
  List<Item> itemList = List();

  @override
  void initState() {
    // TODO: implement initState

    itemList.add(Item(text: "Sports", image: "assets/images/bike.png"));
    itemList.add(Item(text: "Science", image: "assets/images/atom.png"));
    itemList.add(Item(text: "Technology", image: "assets/images/computer.png"));
    itemList.add(Item(text: "Buisness", image: "assets/images/stats.png"));
    itemList.add(Item(text: "Beauty", image: "assets/images/makeup.png"));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppConstants.appbar("Episodes"),
      body: _body(),
    );
  }

  _body() {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.black45,
                offset: Offset(1, 2),
                blurRadius: 10,
                spreadRadius: 2)
          ],
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20))),
      child: Column(
        children: [
          _searchBar()
,Container(
      padding: EdgeInsets.fromLTRB(10,5,10,5),
  child:   Row(

              children: [

                Icon(Icons.location_searching,color: Colors.blue,)
,SizedBox(width: 10,)
                ,Text("Your location",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),)

              ],

            ),
)          ,
          Expanded(child:
          _gridList())
        ],
      ),
    );
  }

  _gridList() {
    return Container(

        child:
        StaggeredGridView.countBuilder(
          crossAxisCount: 4,
          itemCount: 8,
          itemBuilder: (BuildContext context, int index) => new Container(
            margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
            decoration: BoxDecoration(
                color: Colors.blueGrey[50],
                image: DecorationImage(
                    image: NetworkImage(
                      "https://picsum.photos/200/300",
                    ),
                    fit: BoxFit.cover),
                borderRadius: BorderRadius.circular(20)),
          ),
          staggeredTileBuilder: (int index) =>
          new StaggeredTile.count(2, index.isEven ? 2 : 1),
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
        )

//      StaggeredGridView.countBuilder(
//          crossAxisCount: 2,
//
//
//          itemCount: itemList.length,
//          staggeredTileBuilder: (int index) =>
//              new StaggeredTile.count(2,  1 ,),
//          mainAxisSpacing: 4.0,
//          crossAxisSpacing: 4.0,
//          itemBuilder: (BuildContext context, int index) {
//            return Container(
//              margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
//              decoration: BoxDecoration(
//                  color: Colors.blueGrey[50],
//                  image: DecorationImage(
//                      image: NetworkImage(
//                        "https://picsum.photos/200/300",
//                      ),
//                      fit: BoxFit.cover),
//                  borderRadius: BorderRadius.circular(20)),
//            );
//          }),
    );
  }
//  _gridList() {
//    return Container(
//
//      child: GridView.builder(
//          padding: EdgeInsets.all(10),
//          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
//              crossAxisCount: 2,
//              crossAxisSpacing: 1,
//              mainAxisSpacing: 1,
//              childAspectRatio: 1.4),
//          itemCount: itemList.length,
//          itemBuilder: (BuildContext context, int index) {
//            return Container(
//              margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
//              decoration: BoxDecoration(
//                  color: Colors.blueGrey[50],
//                  image: DecorationImage(
//                      image: NetworkImage(
//                        "https://picsum.photos/200/300",
//                      ),
//                      fit: BoxFit.cover),
//                  borderRadius: BorderRadius.circular(20)),
//            );
//          }),
//    );
//  }
  _searchBar(){
    return  Container(
      height: 40,
      margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
      child: TextFormField(
        decoration: InputDecoration(
            suffixIcon: Icon(Icons.arrow_forward_ios,size: 18,color: Colors.blue.withOpacity(0.5),),
            contentPadding: EdgeInsets.fromLTRB(10, 0,0,0),
            hintText: "Enter Location",
            hintStyle: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w400
            ),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                borderSide: BorderSide(color: Colors.blue.withOpacity(0.2))
            )
            ,focusedBorder:OutlineInputBorder(
            borderRadius: BorderRadius.circular(40),
            borderSide: BorderSide(color: Colors.blue.withOpacity(0.5))


        ),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                borderSide: BorderSide(color: Colors.blue.withOpacity(0.5))
            )
        ),

      ),
    );
  }
}

class Item {
  String image, text;

  Item({this.image, this.text});
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:newsapp/AppConstants/appConstants.dart';
import 'package:newsapp/AppConstants/customTheme.dart';
import 'package:newsapp/Provider/ThemeNotifier.dart';
import 'package:provider/provider.dart';

class EpisodesGridList extends StatefulWidget {
  @override
  _EpisodesGridListState createState() => _EpisodesGridListState();
}

class _EpisodesGridListState extends State<EpisodesGridList> {
  List<Item> itemList = List();
  var _darkTheme = true;
  var themeNotifier;

  @override
  void initState() {
    // TODO: implement initState

    itemList.add(Item(text: "Genere", image: "assets/images/bike.png"));
    itemList.add(Item(text: "News", image: "assets/images/atom.png"));
    itemList.add(Item(text: "People", image: "assets/images/computer.png"));
    itemList.add(Item(text: "Buisness", image: "assets/images/stats.png"));
    itemList.add(Item(text: "Beauty", image: "assets/images/makeup.png"));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    themeNotifier = Provider.of<ThemeNotifier>(context);
    _darkTheme = (themeNotifier.getTheme() == darkTheme);
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppConstants.appbar("Episodes"),
      body: _body(),
    );
  }

  _body() {
    return Container(
      decoration: BoxDecoration(
          color: _darkTheme ? Colors.black : Colors.white,
          boxShadow: [
            BoxShadow(
//                color: Colors.black45,
                color: _darkTheme ? Colors.black12 : Colors.black45,
                offset: Offset(1, 5),
                blurRadius: 10,
                spreadRadius: 2)
          ],
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20))),
      child: GridView.builder(
          padding: EdgeInsets.all(15),
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 1,
              mainAxisSpacing: 1,
              childAspectRatio: 1.5),
          itemCount: itemList.length,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: () {
                if (index == 0) {
                  Navigator.pushNamed(context, "/genereTemporary");
                }
                if (index == 1) {
                  Navigator.pushNamed(context, "/newsTemporary");
                }
              },
              child: Container(
                margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                decoration: BoxDecoration(
                    color: Colors.blueGrey[50],
                    borderRadius: BorderRadius.circular(20)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      itemList[index].image,
                      fit: BoxFit.cover,
                      height: 50,
                      width: 50,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      itemList[index].text,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                          letterSpacing: 1),
                    )
                  ],
                ),
              ),
            );
          }),
    );
  }
}

class Item {
  String image, text;

  Item({this.image, this.text});
}

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:newsapp/AppConstants/appConstants.dart';
import 'package:newsapp/AppConstants/customTheme.dart';
import 'package:newsapp/Model/genreResponse.dart';
import 'package:http/http.dart' as http;
import 'package:newsapp/Provider/ThemeNotifier.dart';

import 'package:newsapp/Utils/ApiManger.dart';
import 'package:provider/provider.dart';

class NewsTemporaryScreen extends StatefulWidget {
  @override
  _NewsTemporaryScreenState createState() => _NewsTemporaryScreenState();
}

class _NewsTemporaryScreenState extends State<NewsTemporaryScreen> {
  GenreResponse response;
  List<Articles>articles = List();
  var _darkTheme = true;
  var themeNotifier;
  bool isLoading = false;
  getNews() async {
    setState(() {
      isLoading =true;
    });
    response = GenreResponse.fromJson(await ApiManager(context)
        .getCall("https://blue-pigeon-backend-latest.herokuapp.com/api/news/sources?sources=cnn&page=2&pageSize=20",context));

    if (response.success) {
      print(response);
      articles = response.data.articles;
      setState(() {
        isLoading =false;
      });
//        MultimediaCountStream().setmultimediaCount(response);
    }

  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getNews();
    // getCart();
  }

  @override
  Widget build(BuildContext context) {
    themeNotifier = Provider.of<ThemeNotifier>(context);
    _darkTheme = (themeNotifier.getTheme() == darkTheme);
    return Stack(
      children: [
        Scaffold(
      backgroundColor: Colors.transparent,
          appBar:
        AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: Colors.transparent,
          actionsIconTheme: IconThemeData(color: _darkTheme?Colors.white:Colors.black),
          elevation: 0,
          title: Text(
            "News",
            style: TextStyle(
                fontSize: 25, fontWeight: FontWeight.bold, color: Colors.blue),
          ),
        ),
        body: _body(),),
        AppConstants.progress(isLoading)
      ],
    );
  }

  _body() {
    return Container(
      child: ListView.builder(
          itemCount: articles.length,
          itemBuilder: (BuildContext context,int index){
            return itemView(index);
          }),
    );
  }
  itemView(int index) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
//        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: articles[index].urlToImage == null ?Image.asset(
                  "assets/images/img1.jpg",
                  height: 100,
                  width: 100,
                  fit: BoxFit.cover,
                ):Image.network(
                  articles[index].urlToImage,
                  height: 100,
                  width: 100,
                  fit: BoxFit.cover,
                )
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    articles[index].title ?? " ",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25,color: _darkTheme?Colors.white:Colors.black),
                  ),
                  SizedBox(height: 5,),
                  Text(
                    articles[index].description??" ",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                        color:_darkTheme?Colors.grey[400]: Colors.grey[400]),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

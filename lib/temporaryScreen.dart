import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_paginator/flutter_paginator.dart';
import 'package:newsapp/AppConstants/appConstants.dart';
import 'package:newsapp/AppConstants/customTheme.dart';
import 'package:newsapp/Model/genreResponse.dart';
import 'package:http/http.dart' as http;
import 'package:newsapp/Provider/ThemeNotifier.dart';

import 'package:newsapp/Utils/ApiManger.dart';
import 'package:newsapp/Widgets/paginationWidget.dart';
import 'package:provider/provider.dart';


class TemporaryScreen extends StatefulWidget {
  @override
  _TemporaryScreenState createState() => _TemporaryScreenState();
}

class _TemporaryScreenState extends State<TemporaryScreen> {
  GenreResponse response;
  List<Articles>articles = List();
  var _darkTheme = true;
  var themeNotifier;
  int page = 0;
  int valueKey = 0;
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();
  int listlength = 1;
  bool isLoading = false;
  GlobalKey<PaginatorState> paginatorGlobalKey = GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getgenre();
    // getCart();
  }
  Future<List<Articles>> getgenre() async {
    if(mounted)
    setState(() {
      isLoading = true;
    });
    page = page + 1;
      response = GenreResponse.fromJson(await ApiManager(context)
          .getCall("https://blue-pigeon-backend-latest.herokuapp.com/api/news/genre?genre=politics&page=1&pageSize=20",context));

    if (response != null) {
      if (response.success ) {
        if (response.data.articles.length > 0) {
          if (mounted) {
            setState(() {
              isLoading = false;
              listlength = listlength + response.data.articles.length;
              articles = response.data.articles;
              print(listlength.toString());
            });
          }
          return response.data.articles;
        } else {
          setState(() {
            isLoading = false;
          });
          nodatalogic(page);
        }
      } else {
        setState(() {
          isLoading = false;
        });
        nodatalogic(page);
      }
    } else {
      setState(() {
        isLoading = false;
      });
      nodatalogic(page);
    }
  }

  List<Articles> nodatalogic(int page) {
    if (page > 1) {
      return [];
    } else {
      if (mounted) {
        setState(() {
          listlength = 0;
        });
      }
      return [];
    }
  }



  @override
  Widget build(BuildContext context) {
    themeNotifier = Provider.of<ThemeNotifier>(context);
    _darkTheme = (themeNotifier.getTheme() == darkTheme);
    return Stack(
      children: [
        Scaffold(
backgroundColor: Colors.transparent,
          appBar:AppBar(
            automaticallyImplyLeading: true,
            backgroundColor: Colors.transparent,
            actionsIconTheme: IconThemeData(color: _darkTheme?Colors.white:Colors.black),
            elevation: 0,
            title: Text(
              "Genre",
              style: TextStyle(
                  fontSize: 25, fontWeight: FontWeight.bold, color: Colors.blue),
            ),
          ),
          body:
//          RefreshIndicator(
//            key: _refreshIndicatorKey,
//            onRefresh: () async {
//              if (mounted)
//                setState(() {
//                  valueKey = valueKey + 1;
//                  listlength = 1;
//                  page = 0;
//                });
//            },
//            child:
            _body(),
//          ),
        ),
        AppConstants.progress(isLoading)
      ],
    );
  }

  _body() {
    return
//      Container(
////height: 100,
//        child: listlength > 0
//            ? _paginatedList()
//            : ListView(
//          children: <Widget>[
//            Container(
//                child: Center(
//                  child: Text("no data"),
//                ))
//          ],
//        ),
//      );
//    );

    Container(
      child: ListView.builder(
          itemCount: articles.length,
          itemBuilder: (BuildContext context,int index){
        return itemView(index,articles[index]);
      }),
    );
  }
  itemView(int index,Articles item) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
//        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: item.urlToImage == null ?Image.asset(
                "assets/images/img1.jpg",
                height: 100,
                width: 100,
                fit: BoxFit.cover,
              ):Image.network(
                item.urlToImage,
                height: 100,
                width: 100,
                fit: BoxFit.cover,
              )
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    item.title ?? " ",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25,color: _darkTheme?Colors.white:Colors.black),
                  ),
                  Text(
                    item.description??" ",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                        color:_darkTheme?Colors.grey[400]: Colors.grey[400]),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

//  _paginatedList() {
//    print("1");
//    return Pagination<Articles>(
//      physics: const AlwaysScrollableScrollPhysics(),
//      key: ValueKey(valueKey),
//      padding: EdgeInsets.only(top: 8, bottom: 8),
//      progress: AppConstants.progress(isLoading),
//      pageBuilder: (currentSize) => getgenre(),
//      itemBuilder: (index, item) {
//        print("dfjasfkjdjkfjkajkdsfc  "+item.toString());
//        return  itemView(index,item) ;
//      },
//    );
//  }
//

}

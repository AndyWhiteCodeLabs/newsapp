import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ApiManager {
  BuildContext context;

  ApiManager(BuildContext context) {
    this.context = context;
  }
  postCall(String url, Map request, BuildContext context) async {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    var headers;
//    if (prefs.getString(AppStrings.ACCESSTOKEN_PREF_KEY) == null) {
////      headers = {"Accept": "application/json"};
////    } else {
//      AccessTokenResponse user = AccessTokenResponse.fromJson(
//          jsonDecode(prefs.getString(AppStrings.ACCESSTOKEN_PREF_KEY)));
//      headers = {
//        "Accept": "application/json",
////        "Authorization": user.data.tokenType + " " + user.data.accessToken
////      };
//    };
//    print(headers);
    print("URL " + url);
    print(request);

    http.Response response =
    await http.post(url, body: request);
    if (response.statusCode == 401) {
//      AppConstants.logout(context);
    } else {
//      AtarkeLogs.debugging("${response.statusCode}");
//      AtarkeLogs.debugging("${response.body}");
      return await json.decode(response.body);
    }
  }
  getCall(
      String url, BuildContext context) async {
    var headers;


      headers = {
        "Accept": "application/json",
      };
    var uri = Uri.parse(url);
//    uri = uri.replace(queryParameters: request);
    print(uri);
    print(headers);
    http.Response response = await http.get(url, headers: headers);
    if (response.statusCode == 401) {
          print(headers);

    } else {
      print("${response.statusCode}");
      print("${response.body}");
      return await jsonDecode(response.body);
    }
  }
}
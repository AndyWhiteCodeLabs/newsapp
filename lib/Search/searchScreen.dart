import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> with TickerProviderStateMixin {
  TabController _tabController;
  TextEditingController searchController = new TextEditingController();
  var _darkTheme = true;
  List<String> networkImage = [
    "https://picsum.photos/200/300",
    "https://picsum.photos/200",
    "https://picsum.photos/200",
    "https://picsum.photos/200",
    "https://picsum.photos/200/400",
  ];

  void initState() {
    _tabController = new TabController(length: 5, vsync: this);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Icon(FontAwesomeIcons.angleLeft, color: Colors.blue,size:22,),
                  Text(
                    "Search",
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0.5,
                        color:_darkTheme?Colors.white54: Colors.black),
                  )
                ],
              ),
              Container(
                child: Text(
                  "Episodes",
                  style: TextStyle(color: Colors.blue,fontSize: 25,fontWeight: FontWeight.bold,letterSpacing: 0.5),
                ),
              ),
              Container(),
              Container()
            ],
          ),
        ),
      ),
      body: _body(),
    );
  }
  _body() {
    return Container(

      child: DefaultTabController(
        length: 5,
        child: Scaffold(
          backgroundColor: Colors.transparent,

          appBar: PreferredSize(
            preferredSize: Size.fromHeight(200),
            child: TabBar(

              indicator: new BubbleTabIndicator(
                indicatorHeight: 30.0,
                indicatorColor: Colors.grey,
                tabBarIndicatorSize: TabBarIndicatorSize.tab,
              ),
              controller: _tabController,
              isScrollable: true,
              indicatorColor: Colors.transparent,
              unselectedLabelColor: Colors.grey,
//              labelPadding: EdgeInsets.only(left:20.0),
              indicatorPadding: EdgeInsets.only(left:20.0),
              labelStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
              unselectedLabelStyle:
              TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
              indicatorSize: TabBarIndicatorSize.tab,
              labelColor: _darkTheme ? Colors.white:Colors.black,
              indicatorWeight: 2,
              tabs: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(0, 15, 5, 15),
                  child: Text(
                    "Top result",
//                  style: TextStyle(
//                      fontSize: 18, fontWeight: FontWeight.w700),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 15, 5, 15),
                  child: Text(
                    "Blog",
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 15, 5, 15),
                  child: Text(
                    "Articles",
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 15, 5, 15),
                  child: Text(
                    "Starred",
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 15, 5, 15),
                  child: Text(
                    "Draft",
                  ),
                ),
              ],
            ),
          ),
          body: TabBarView(
            dragStartBehavior: DragStartBehavior.down,
            controller: _tabController,
            children: <Widget>[
//              _inboxContainer(),
              _topResult(),
              Center(
                child: Container(
                  child: Text("hi "),
                ),
              ),
              Center(
                child: Container(
                  child: Text("hi "),
                ),
              ),
              Center(
                child: Container(
                  child: Text("hi "),
                ),
              ),
              Center(
                child: Container(
                  child: Text("hi "),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _topResult() {
    return Container(
      child: ListView.builder(
          itemCount: 5,
          itemBuilder: (BuildContext context, int index) {
            return itemView(index);
          }),
    );
  }
  itemView(int index) {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
      decoration: BoxDecoration(
        color:_darkTheme?Colors.white12: Colors.blueGrey.shade50,
borderRadius: BorderRadius.circular(20)
      ),
      child: Row(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(5, 5, 0, 5),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Image.network(
                networkImage[index],
                height: 100,
                width: 100,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Headline ",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25,color: _darkTheme?Colors.white70:Colors.black),
                  ),
                  Text(
                    "Lorem ipsum dolor sitamet, consetetursadipscing elitr, seddiam nonumy eirmod",
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        color: Colors.grey[600]),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

}

import 'package:carousel_pro/carousel_pro.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:newsapp/AppConstants/customTheme.dart';
import 'package:newsapp/Provider/ThemeNotifier.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<String> imageList = [
    "assets/images/img1.jpg",
    "assets/images/image2.jpg",
    "assets/images/img3.jpg"
  ];
    List<String> networkImage = [
      "https://picsum.photos/200/300",
      "https://picsum.photos/200",
      "https://picsum.photos/200",
      "https://picsum.photos/200",
      "https://picsum.photos/200/400",
    ];
  int _current = 0;
  var _darkTheme = true;
  var themeNotifier;
  @override
  Widget build(BuildContext context) {
    themeNotifier = Provider.of<ThemeNotifier>(context);
    _darkTheme = (themeNotifier.getTheme() == darkTheme);
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "Episodes",
          style: TextStyle(
              fontSize: 25, fontWeight: FontWeight.bold, color: Colors.blue),
        ),
      ),
      body: _body(),
    );
  }

  _body() {
    return Container(
      child: Column(
        children: [_imageSlider(), Expanded(child: _mainBody())],
      ),
    );
  }

  _mainBody() {
    return Container(
      child: ListView.builder(
          itemCount: networkImage.length,
          itemBuilder: (BuildContext context, int index) {
        return itemView(index);
      }),
    );
  }
//  _mainBody() {
//    return Container(
//      child: StaggeredGridView.countBuilder(
//          crossAxisCount: 3,
//
//          itemCount: networkImage.length,
//          staggeredTileBuilder: (int index) =>
//              new StaggeredTile.count(4, index.isEven ? 1 : 1),
//          mainAxisSpacing: 4.0,
//          crossAxisSpacing: 4.0,
//          itemBuilder: (BuildContext context, int index) {
//            return itemView(index);
//          }),
//    );
//  }

    itemView(int index) {
      return Container(
        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: Row(
          children: [
            Container(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Image.network(
                  networkImage[index],
                  height: 100,
                  width: 100,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Headline for the news",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25,color: _darkTheme?Colors.white70:Colors.black),
                    ),
                    Text(
                      "This is a sample textline for the news content.This is a sample line for the news content.",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 18,
                          color:_darkTheme?Colors.white54: Colors.grey[400]),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

  _imageSlider() {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: CarouselSlider(
            options: CarouselOptions(
                autoPlay: false,
                aspectRatio: 2.5,
                enlargeCenterPage: true,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                }),

//                onImageTap: (imageindex) {},
//                defaultImage: Image.asset("assets/images/nikeShoes.png"),
//                indicatorBgPadding: 6,
//                dotIncreaseSize: 1.4,
//                boxFit: BoxFit.fitHeight,
//                dotColor: Colors.black,
//                showIndicator: false,
//                autoplay: false,
//                animationCurve: Curves.linearToEaseOut,
//                dotBgColor: Colors.black12,
            items: imageList
                .map((item) => Stack(

                  children: [
                    Container(
//                          margin: EdgeInsets.fromLTRB(20, 20, 20, 5),
                          // color: Colors.blue,
//                  height: MediaQuery.of(context).size.height,
//                  width: MediaQuery.of(context).size.width*0.2,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              image: DecorationImage(
                                  image: evictImage(item), fit: BoxFit.cover)),
                        ),
                    Positioned(
                      top: 125,
                        left: 20,
                        child: Container(

                          child: Text("Genres",style: TextStyle(color: Colors.white,fontSize: 25,fontWeight: FontWeight.w600),),
                    ))
                  ],
                ))
                .toList(),
          ),
        ),
        imageList.length > 1
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: imageList.map((url) {
                  int index = imageList.indexOf(url);
                  return Container(
                    width: 8.0,
                    height: 8.0,
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _current == index
                          ? Colors.blue
                          : _darkTheme?Colors.white54:Color.fromRGBO(0, 0, 0, 0.4),
                    ),
                  );
                }).toList(),
              )
            : Container(),
      ],
    );
  }

  AssetImage evictImage(String url) {
    final AssetImage provider = AssetImage(url);
    // if (!widget.cacheimage) {
    //   provider.evict().then<void>((bool success) {
    //     if (success) debugPrint('removed image!');
    //   });
    // }
    return provider;
  }
}

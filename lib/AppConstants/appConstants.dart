import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:newsapp/AppConstants/colors.dart';

class AppConstants{

  static appbar(String text){
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Colors.transparent,
      elevation: 0,
      title: Text(
        text,
        style: TextStyle(
            fontSize: 25, fontWeight: FontWeight.bold, color: Colors.blue),
      ),
    );

  }

  static progress(bool isLoading) {
    return isLoading
        ? Container(
//      height: double.infinity,
//      width: double.infinity,
      color: Color(0x80ffffff),
      child: SpinKitThreeBounce(
        size: 50,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            padding: EdgeInsets.only(right: 5, left: 5),
            child: DecoratedBox(
              decoration: BoxDecoration(
                  border: Border.all(width: 0.4),
                  shape: BoxShape.circle,
                  color:  Colors.blue),
            ),
          );
        },
      ),
    )
        : Container();
  }



}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:newsapp/AppConstants/customTheme.dart';
import 'package:newsapp/Provider/ThemeNotifier.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingScreen extends StatefulWidget {
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  var _darkTheme = true;
  var themeNotifier;
  @override
  Widget build(BuildContext context) {
     themeNotifier = Provider.of<ThemeNotifier>(context);
    _darkTheme = (themeNotifier.getTheme() == darkTheme);
    return Scaffold(
      backgroundColor:_darkTheme ? Colors.black: Colors.transparent,
      appBar: AppBar(
        backgroundColor:_darkTheme ? Colors.black: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "Episodes",
          style: TextStyle(
              fontSize: 25, fontWeight: FontWeight.bold, color: Colors.blue),
        ),
      ),
      body: _body(),
    );
  }

  _body() {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Settings",
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: _darkTheme ? Colors.white: Colors.black
            ),
          ),
          _darkThemeSwitch(),
          Expanded(child: _listView())
        ],
      ),
    );
  }

  _darkThemeSwitch(){
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text("Dark Theme",style: TextStyle(color: !_darkTheme ? Colors.black87:Colors.white70,fontSize: 18,fontWeight: FontWeight.w500),)
        , Container(
            child: Switch(
              value: _darkTheme,
              onChanged: (val) {
                setState(() {
                  _darkTheme = val;
                });
                onThemeChanged(val, themeNotifier);
              },
              activeColor: Colors.blue,
            ),
          ),
        ],
      ),
    );
  }
  void onThemeChanged(bool value, ThemeNotifier themeNotifier) async {
    (value)
        ? themeNotifier.setTheme(darkTheme)
        : themeNotifier.setTheme(lightTheme);
    var prefs = await SharedPreferences.getInstance();
    prefs.setBool('darkMode', value);
  }

  _listView(){
    return Container(
      child: ListView.builder(
          itemCount: 10,
          itemBuilder: (BuildContext context,int index){
        return Container(
          padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Primary Text",style: TextStyle(fontWeight: FontWeight.bold,color:_darkTheme ? Colors.white: Colors.grey[700],fontSize: 20),)
              ,
              SizedBox(height: 2,),
              Text("Secondary Text",style: TextStyle(fontWeight: FontWeight.w500,color: Colors.grey,fontSize: 16),),
SizedBox(height: 20,)
              ,Container(
                height: 0.5,
                color: Colors.grey,

              )
            ],
          ),
        );
      }),
    );
  }
}
